<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 
 class M_user extends CI_Model {
 
	public function tambah_user()
	{
		$object=array(
			'nama_pelanggan'=>$this->input->post('nama'),
			'alamat'=>$this->input->post('alamat'),
			'nomor_kwh'=>$this->input->post('nomor_kwh'),
			'username'=>$this->input->post('username'),
			'password'=>md5($this->input->post('password')),
			'id_tarif'=>$this->input->post('id_tarif')
		);
		return $this->db->insert('pelanggan', $object);
	}

 	public function get_login()
 	{
 		return $this->db
 					->where('username', $this->input->post('username'))
 					->where('password', md5($this->input->post('password')))
 					->get('pelanggan');
 	}
 
 }
 /*ini adalah model untuk login*/
 /* End of file M_user.php */
 /* Location: ./application/models/M_user.php */ ?>