<div class="block-header">
    <h2>Data Detail Tagihan <?=$data_pelanggan->nama_pelanggan?></h2>
</div>

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                                     
                <table class="table table-hover table-striped datatable">
                  <thead>
                    <tr>
                        <th>BULAN</th><th>TAHUN</th><th>TOTAL PENGGUNAAN</th><th>STATUS</th>
                    </tr>
                   </thead>
                   <tbody>
                    <?php 
                      foreach ($data_detail as $Penggunaan) {
                          echo '<tr>
                                  <td>'.$Penggunaan->bulan.'</td>
                                  <td>'.$Penggunaan->tahun.'</td>
                                  <td>'.$Penggunaan->jumlah_meter.'</td>  
                                  <td>'.$Penggunaan->status.'</td>  
                               </tr>';
                      }
                      ?>
                   </tbody>
                   
                    
                 </table>


                <?php 
                  if($this->session->flashdata('pesan')!=null){
                    echo $this->session->flashdata('pesan');
                  }
                ?>
            </div>
        </div>
    </div>
</div>


                            

<script>
  $(".datatable").dataTable();
</script>
