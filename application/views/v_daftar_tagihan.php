<div class="block-header">
    <h2>Data Detail Tagihan </h2>
</div>

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
              <table class="table table-hover table-striped datatable">
                  <thead>
						<tr>
							<th>ID TAGIHAN</th>
							<th>BULAN</th>
							<th>TAHUN</th>
							<th>KWH PENGGUNAAN</th>
							<th>GRAND TOTAL</th>
							<th>BUKTI</th>
							<th>STATUS</th>
							<th>AKSI</th>
						</tr>
					</thead>
					<tbody>
						<?php 
	                     	foreach ($tagihan as $tag):
							$cek_bayar=$this->tagihan->cek_pembayaran($tag->id_tagihan);
						 ?>
							<tr>
								<td><?=$tag->id_tagihan?></td>
								<td><?=$tag->bulan?></td>
								<td><?=$tag->tahun?></td>
								<td><?=$tag->jumlah_meter?></td>
								<td><?=($tag->tarifperkwh*$tag->jumlah_meter+2500)?></td>
								<td>
									<?php
									if(@$cek_bayar->bukti!=""){
										echo '<img src="'.base_url().'assets/bukti/'.$cek_bayar->bukti.'" width="40">';
									}
									?>
								</td>
								<td>
									<?php 
									if(@$cek_bayar->status==null){
										echo $tag->status;
									} else{
										echo $cek_bayar->status;
									}
									?>	
								</td>
								<td>
									<?php 
									if(@$cek_bayar->status=='lunas'){
										echo 'LUNAS';
									} else{
										echo '<a href="#upload" data-toggle="modal" onclick=bayar('.$tag->id_tagihan.') class="btn btn-info">UPLOAD</a>';
									}
									?>
									</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				<?=$this->session->flashdata('pesan');?>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id=upload>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Upload Bukti Pembayaran</h4>
      </div>
      <div class="modal-body">
		<form method="post" action="<?=base_url('index.php/trans/upload_bukti')?>" enctype="multipart/form-data"> 
			<input type="file" name="bukti" class="form-control"><br>
			<input type="hidden" name="id_tagihan" id="id_tagihan">
			<input type="submit" name="submit" value="Kirim" class="btn btn-success" style="float:left;margin-right:10px">
       
       	</form>
       	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	function bayar(id_tagihan){
		$("#id_tagihan").val(id_tagihan);
	}

  $(".datatable").dataTable({
    dom: 'Bfrtip',
    responsive: true,
    buttons: [
      'print'
    ]
  });
</script>




