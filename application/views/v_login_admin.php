<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login Admin</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?=base_url()?>assets/img/favicon.png">
</head>
<!--
	ini adalah view untuk loginnya sisi admin
-->
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="alert alert-success" id="msg" style="display: none;"></div>
								<h1>Login Admin</h1>
							</div>
							<?php 
  								if($this->session->flashdata('pesan')!=null){
  									echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";
  								}
  							?>
							<form class="form-auth-small" action="<?= base_url('index.php/admin/cek_login_admin')?>" id="signin" method="post">
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input name="username" type="text" class="form-control" id="signin-email" placeholder="Username">
								</div>
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input name="password" type="password" class="form-control" id="signin-password" placeholder="Password">
								</div>
								<div class="col-xs-12">
									<!--<input type="hidden" name="login" value="1">-->
									<input name="login" type="submit" id="login" class="btn btn-primary btn-lg btn-block" value="Login">
								</div>


								<div class="col-xs-12">
									<!--<input type="hidden" name="login" value="1">-->
								</div>
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Payment Point Online Bank(PPOB)</h1>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#login").click(function(){
			var data = $('signin').serialize();
			$("msg").html("Memeriksa Data Login...");
			$.ajax({
				url:"<?= base_url()?>index.php/admin/cek_login_admin",
				type:"POST",
				dataType:"json",
				data:data,
				cache:false,
				success:function(dt){
					if (dt.status==1) {
						$("#msg").show('fade');
						$("msg").html('dt.keterangan');
						setTimeout(function(){
							window.location.href="<?= base_url()?>index.php/home_admin";
						},2000);
					}else{
						$("#msg").show('fade');
						$("msg").html('dt.keterangan');
						setTimeout(function(){
							$("msg").hide('fade');
						},2000);
					}
				}
			});
		});

	});
</script>