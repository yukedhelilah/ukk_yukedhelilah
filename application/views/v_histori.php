<div class="block-header">
    <h2>Histori Pemhistorian</h2>
</div>

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
              <table class="table table-hover table-striped datatable">
                  <thead>
                    <tr>
                        <th>NOMOR KWH</th>
                        <th>NAMA PELANGGAN</th>
                        <th>TGL LUNAS</th>
                        <th>BULAN BAYAR</th>
                        <th>TAHUN BAYAR</th>
                        <th>BIAYA ADMIN</th>
                        <th>TOTAL BAYAR</th>
                        <th>STATUS</th>
                        <th>BUKTI</th>
                        <th>VERFIKASI ADMIN</th>
                    </tr>
                   </thead>
                   <tbody>
                    <?php 
                      foreach ($data_histori as $histori) {
                        $dt_admin=$this->admin->detail_admin(@$histori->id_admin);
                          echo '<tr>
                                  <td>'.$histori->nomor_kwh.'</td>
                                  <td>'.$histori->nama_pelanggan.'</td>
                                  <td>'.$histori->tanggal_pembayaran.'</td>  
                                  <td>'.$histori->bulan.'</td>
                                  <td>'.$histori->tahun.'</td> 
                                  <td>'.$histori->biaya_admin.'</td> 
                                  <td>'.$histori->total_bayar.'</td>
                                  <td>'.$histori->status.'</td>
                                  <td><img src="'.base_url().'assets/bukti/'.$histori->bukti.'" width="40"></td>
                                  <td>'.@$dt_admin->nama_admin.'</td>
                                  
                               </tr>';
                      }
                      ?>
                   </tbody>
                   
                    
                 </table>       
            </div>
        </div>
    </div>
</div>

<script>
  $(".datatable").dataTable({
    dom: 'Bfrtip',
    responsive: true,
    buttons: [
      'print'
    ]
  });
</script>


