<div class="main-content">
        <div class="container-fluid">
          <h3 class="page-title">Data Admin</h3>
          <div class="row">
            
              <!-- TABLE STRIPED -->
              <div class="panel">
              <?php
              $notif = $this->session->userdata('notif');
              if ($notif != NULL) {
                echo '
                <div class="alert alert-danger">'.$notif.'</div>
                ';
              }
            ?>
                
                <div class="panel-body">
                  <table class="table table-striped">

                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Admin</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Admin</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#tambah">Tambah Admin</button>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($data_admin as $dt_admin) {
                        echo '
                          <tr>
                            <td>'.$no.'</td>
                            <td>'.$dt_admin->nama_admin.'</td>
                            <td>'.$dt_admin->username.'</td>
                            <td>'.$dt_admin->password.'</td>
                            <td>'.$dt_admin->nama_level.'</td>
                            <td>
                              <a href="#update_admin" class="btn btn-info" data-toggle="modal" onclick="tm_detail('.$dt_admin->id_admin.')">Update</a> 
                              <a href="'.base_url('index.php/tampilan_admin/hapus_admin/'.$dt_admin->id_admin).'" onclick="return confirm(\'anda yakin?\')" class="btn btn-danger">Delete</a></td>
                             </tr>';
                            $no++;
                      }
                        ?>
                    </tbody>
                  </table>

                <?php 
                  if($this->session->flashdata('pesan')!=null){
                    echo '<div class="alert alert-info">'.$this->session->flashdata('pesan').'</div>';
                  }
                ?>
                </div>
              </div>
              <!-- END TABLE STRIPED -->
              <!-- END CONDENSED TABLE -->
            </div>
          </div>
        </div>
<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Tambah admin</h4>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/tampilan_admin/simpan_admin')?>" method="post" enctype="multipart/form-data">
          Nama admin 
          <input type="text" class="form-control" name="nama_admin">
          <br>
          Username 
          <input type="text" class="form-control" name="username">
          <br>
         Password
          <input type="text" class="form-control" name="password">
          <br>
          Level 
          <select name="id_level" class="form-control">
              <option></option>
              <?php foreach ($data_level as $level): ?>
                  <option value="<?=$level->id_level?>"><?=$level->nama_level?></option>
              <?php endforeach ?>
          </select>
          <br>
          <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="update_admin" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Update admin</h4>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/tampilan_admin/update_admin')?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="id_admin" id="id_admin">
          Nama admin 
          <input id="nama_admin" type="text" name="nama_admin" class="form-control"><br>
          Password 
          <input id="password" type="password" name="password" class="form-control"><br>
           Level 
          <select name="id_level" id="id_level" class="form-control">
              <option></option>
              <?php foreach ($data_level as $level): ?>
                  <option value="<?=$level->id_level?>"><?=$level->nama_level?></option>
              <?php endforeach ?>
          </select><br>
          <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                            

<script>
  
  function tm_detail(id_admin) {
    $.getJSON("<?=base_url()?>index.php/tampilan_admin/get_detail_admin/"+id_admin,function(data){
        $("#id_admin").val(data['id_admin']);
        $("#nama_admin").val(data['nama_admin']);
        $("#username").val(data['username']);
        $("#password").val(data['password']);
        $("#id_level").val(data['id_level']);
        
    });
  }

</script>