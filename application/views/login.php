<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?=base_url()?>assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="alert alert-success" id="msg" style="display: none;"></div>
								<h1>Login User</h1>
							</div>
							<?php 
  								if($this->session->flashdata('pesan')!=null){
  									echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";
  								}
  							?>
							<form class="form-auth-small" action="<?= base_url('index.php/login/proses_login')?>" id="signin" method="post">
								<div class="form-group">
									<label for="signin-email" class="control-label sr-only">Email</label>
									<input name="username" type="text" class="form-control" id="signin-email" placeholder="Username">
								</div>
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input name="password" type="password" class="form-control" id="signin-password" placeholder="Password">
								</div>
								<div class="col-xs-12">
									<!--<input type="hidden" name="login" value="1">-->
									<input name="login" type="submit" id="login" class="btn btn-primary btn-lg btn-block" value="LOGIN">
								</div>


								<div class="col-xs-12">
									<!--<input type="hidden" name="login" value="1">-->
								<a class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#daftar">DAFTAR</a>
								</div>
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Payment Point Online Bank(PPOB)</h1>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="daftar">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Pendaftaran Pelanggan</h4>
          </div>
          <div class="modal-body">
            <form action="<?=base_url('index.php/login/simpan')?>" method="post">
            <table style="width:100%">
                <tr>
                    <td>Nama</td><td><input required type="text" name="nama" class="form-control"></td>
                </tr>
                <tr>
                    <td>Alamat</td><td><textarea required name="alamat" class="form-control"></textarea></td>
                </tr>
                <tr>
                    <td>Nomor KWH</td><td><input required type="text" name="nomor_kwh" class="form-control"></td>
                </tr>
                <tr>
                    <td>Daya</td>
                    <td>
                        <select name="id_tarif" class="form-control">
                            <option></option>    
                            <?php foreach ($tarif as $tarif): ?>
                                <option value="<?=$tarif->id_tarif?>"><?=$tarif->daya?></option>
                            <?php endforeach ?>
                        </select>
                    </td>
                </tr>
                 <tr>
                    <td>Username</td><td><input required type="text" name="username" class="form-control"></td>
                </tr>
                <tr>
                    <td>Password</td><td><input required type="password" name="password" class="form-control"></td>
                </tr>
            </table>
            <input type="submit" class="btn btn-info" name="daftar" value="DAFTAR">
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	<!-- END WRAPPER -->

    <!-- Jquery Core Js -->
    <script src="<?=base_url()?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?=base_url()?>assets/js/admin.js"></script>
    <script src="<?=base_url()?>assets/js/pages/examples/sign-in.js"></script>
</body>

</html>
<script type="text/javascript">
	$(document).ready(function(){
		$("#login").click(function(){
			var data = $('signin').serialize();
			$("msg").html("Memeriksa Data Login...");
			$.ajax({
				url:"<?= base_url()?>index.php/login/proses_login",
				type:"POST",
				dataType:"json",
				data:data,
				cache:false,
				success:function(dt){
					if (dt.status==1) {
						$("#msg").show('fade');
						$("msg").html('dt.keterangan');
						setTimeout(function(){
							window.location.href="<?= base_url()?>index.php/user";
						},2000);
					}else{
						$("#msg").show('fade');
						$("msg").html('dt.keterangan');
						setTimeout(function(){
							$("msg").hide('fade');
						},2000);
					}
				}
			});
		});

	});
</script> <!--yang saya blok itu adalah proses untuk loginnya-->