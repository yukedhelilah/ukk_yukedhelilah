<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('login')!=TRUE){
				redirect('login','refresh');
			}
	}

	public function index()
	{
		$data['konten'] = "v_home";
		$data['judul'] = "home";
		$this->load->view('template_user', $data);
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login','refresh');
	}
}
