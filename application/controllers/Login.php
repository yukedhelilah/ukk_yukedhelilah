<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('tarif_model','tarif');
	}

	public function index()
	{
		if ($this->session->userdata('login')==TRUE) {
			redirect('user','refresh');
		}
		else{
			$data['tarif']=$this->tarif->get_tarif();
			$this->load->view('login',$data);
		}
	}

	public function proses_login()
	{
		if ($this->input->post('login')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run() == TRUE ) {
				$this->load->model('m_user');
				if ($this->m_user->get_login()->num_rows()>0) {
					$data=$this->m_user->get_login()->row();
					$array = array(
						'login' => TRUE,
						'nama_pelanggan'=>$data->nama_pelanggan,
						'username'=>$data->username,
						'password'=>$data->password,
						'id_pelanggan'=>$data->id_pelanggan
						 );
					$this->session->set_userdata($array);
					redirect('user','refresh');
				}
				else {
				$this->session->set_flashdata('pesan', 'salah username atau password');
				redirect('login','refresh');
				}
			}
		}else{
				$this->session->set_flashdata('pesan', validation_errors());
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[pelanggan.username]',
			array('required' => 'Username harus diisi'));

		if ($this->form_validation->run() == TRUE) {
			$this->load->model('m_user','m');
			$masuk=$this->m->tambah_user();
			if($masuk==true){
				$this->session->set_flashdata('pesan', 'sukses menambahkan pelanggan');
			} else {
				$this->session->set_flashdata('pesan', 'username tidak tersedia');
			}
			redirect(base_url('index.php/login'),'refresh');
		} else {
			$this->session->set_flashdata('pesan', 'username tidak tersedia');
			redirect(base_url('index.php/login'),'refresh');

		}
	}

}